import React from 'react';

import Stopwatch from "./client/stopwatch/components/Stopwatch";

import './App.css';

function App() {

  return (
    <div className="App">
     <Stopwatch />
    </div>
  );
}

export default App;
