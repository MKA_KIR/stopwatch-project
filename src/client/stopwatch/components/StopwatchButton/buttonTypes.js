export const buttonTypes = {
    "start": "stopwatch-btn-start",
    "stop": "stopwatch-btn-stop",
    "reset": "stopwatch-btn-reset",
    "wait": "stopwatch-btn-wait",
};