import React from 'react';
import PropTypes from 'prop-types';

import "./StopwatchButton.scss";

import {buttonTypes} from "./buttonTypes";

const StopwatchButton = ({text, type, onClick}) => {
    return (
        <button className={`stopwatch-btn ${buttonTypes[type]}`} onClick={onClick}>
            {text}
        </button>
    );
};

export default StopwatchButton;

StopwatchButton.defaultProps = {
    onClick: () => {},
}

StopwatchButton.propTypes = {
    text: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['start', 'stop', "reset", "wait"]).isRequired,
    onClick: PropTypes.func
};

