import React from 'react';
import PropTypes from "prop-types";

import StopwatchButton from "../StopwatchButton";

import './ButtonGroup.scss';

const ButtonGroup = (props) => {
    const {start, stop, reset, wait} = props;
    return (
        <div className='stopwatch'>
            <StopwatchButton text="Start" type="start" onClick={start} />
            <StopwatchButton text="Stop" type="stop" onClick={stop} />
            <StopwatchButton text="Reset" type="reset" onClick={reset} />
            <StopwatchButton text="Wait" type="wait" onClick={wait} />
        </div>
    );
};

export default ButtonGroup;

ButtonGroup.defaultProps = {
    start: () => {},
    stop: () => {},
    reset: () => {},
    wait: () => {},
};

ButtonGroup.propTypes = {
    start: PropTypes.func,
    stop: PropTypes.func,
    reset: PropTypes.func,
    wait: PropTypes.func,
};

