import React, {useState, useEffect} from "react";

import Time from "../Time";
import ButtonGroup from "../ButtonGroup";

const Stopwatch = () => {
    const [isRunning, setIsRunning] = useState(false);
    const [time, setTime] = useState({s:0, m:0, h:0});
    const [waitTime, setWaitTime] = useState(null);

    useEffect(() => {
        let intervalId;

        if (isRunning) {
            intervalId = setInterval(() => {
                let {s, m, h} = time;
                s++;
                if(s === 60){
                    m++;
                    s = 0;
                }
                if(m === 60){
                    h++;
                    m = 0;
                }

                setTime({s, m, h});
            }, 1000)
        }

        return () => clearInterval(intervalId);
    }, [isRunning, time])

    const start = ()=> setIsRunning(true);
    const stop = ()=> {
        setIsRunning(false);
        setTime({s:0, m:0, h:0});
    }
    const reset = ()=> {
        setTime({s:0, m:0, h:0});
    };
    const wait = ()=> {
        const now = new Date();
        const diff = now - waitTime;
        if(diff < 300){
            setIsRunning(false);
            setWaitTime(null)
        }
        else {
            setWaitTime(now)
        }
    }

    return (
        <div>
            <Time time={time}/>
            <ButtonGroup wait={wait} reset={reset} stop={stop} start={start}/>
        </div>
    );
}

export default Stopwatch;
